package lceb

import com.raquo.laminar.api.L.*
import io.laminext.syntax.core.thisEvents
import io.laminext.tailwind.theme.Theme
import io.laminext.tailwind.theme.DefaultTheme
import io.laminext.syntax.tailwind.*
import org.scalajs.dom
import org.scalajs.dom.document
import UiInputs.inputNumberWithLabel
import lceb.domain.*

import scala.util.Random

object MainPage {
  val $problem: Var[Problem] = Var(Problem.random)
  val solutions = new EventBus[Computation]
  Theme.setTheme(DefaultTheme.theme)

  def main(args: Array[String]): Unit = {
    document.addEventListener(
      "DOMContentLoaded",
      { (_: dom.Event) =>
        setupUI()
      }
    )
  }

  def setupUI(): Unit = {
    val body = dom.document.querySelector("body")
    body.innerHTML = ""
    render(body, div(page))
  }

  val page: HtmlElement = div(
    div.card.title("Input"),
    div.card.body(
      onMountInsert { implicit ctx =>
        inputNumberWithLabel(zoomForTarget)(
          "target",
          "Total",
          1,
          (_, v) => v.toInt
        )
      },
      (1 to 6).map(i =>
        onMountInsert { implicit ctx =>
          div(inputNumberWithLabel(zoomForNumber(i - 1))(
            s"id$i",
            s"I$i",
            1,
            (_, v) => v.toInt
          ))
        }
      )
    ),
    button.btn.xs.fill.blue(
      "Calculer",
      thisEvents(onClick).map(_ => Solver.solveAll($problem.now()).head) --> solutions
    ),
    div.card.title("Output"),
    div.card.body(
      child <-- solutions.events.map(c => span(c.toString))
    )
  )

  def zoomForTarget(implicit context: MountContext[_]): Var[Int] =
    $problem.zoom(_.target)($problem.now().withTarget)(context.owner)

  def zoomForNumber(pos: Int)(implicit context: MountContext[_]): Var[Int] =
    $problem.zoom(_.numbers(pos))($problem.now().withNumber(pos))(context.owner)

}
