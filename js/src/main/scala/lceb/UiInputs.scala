package lceb

import com.raquo.airstream.core.Signal
import com.raquo.laminar.api.L.*
import com.raquo.laminar.nodes.ReactiveHtmlElement
import org.scalajs.dom.raw.HTMLElement

object UiInputs {
  def inputNumberWithLabel[T](variable: Var[T])(
    id: String,
    text: String,
    step: Double,
    update: (T, String) => T
  ): Seq[ReactiveHtmlElement[HTMLElement]] =
    Seq(
      span(
        label(
          cls := "w-40",
          forId := id,
          text
        ),
        input(
          cls := "w-20 bg-gray-50 border border-gray-300 text-gray-900 text-sm focus:ring-blue-500 focus:border-blue-500",
          `type` := "number",
          stepAttr := step.toString,
          idAttr := id,
          value <-- variable.signal.map(_.toString),
          onChange.mapToValue --> variable.updater(update)
        )
      )
    )
}
