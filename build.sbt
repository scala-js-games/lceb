lazy val lcebFull = project
  .in(file("."))
  .aggregate(lceb.js)
  .settings(
    publish := {},
    publishLocal := {}
  )

lazy val lceb = crossProject(JSPlatform)
  .in(file("."))
  .settings(
    name := "LCEB",
    scalaVersion := "3.1.2",
    libraryDependencies += "org.scalatest" %%% "scalatest" % "3.2.10" % "test"
  )
  .jsSettings(
    scalaJSUseMainModuleInitializer := true,
    libraryDependencies ++= Seq(
      "io.laminext" %%% "fetch-upickle" % "0.14.3",
      "io.laminext" %%% "tailwind-default-theme" % "0.14.3",
      "com.lihaoyi" %%% "scalatags" % "0.11.1"
    )
  )

Global / onChangedBuildSource := ReloadOnSourceChanges
