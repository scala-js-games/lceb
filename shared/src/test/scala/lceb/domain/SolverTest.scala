package lceb.domain

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class SolverTest extends AnyFlatSpec with Matchers {


  "lceb" should "solve 100-((2+4)x10)+7x50 = 390" in {
    val problem = Problem(390, List(100, 2, 4, 10, 7, 50))
    val Some(s) = Solver.solveAll(problem).headOption
    s.eval.get shouldBe 390
  }
}
