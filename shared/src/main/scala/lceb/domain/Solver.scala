package lceb.domain

import lceb.domain.Operation._


object Solver {

  def solveAll(problem: Problem): List[Computation] = {
    val combinaisons = allSums(problem.numbers)
    val tries = combinaisons.flatMap(useAllOperations)
    tries.filter(problem.target == _.eval.getOrElse(0))
  }

  private def useAllOperations(c: Computation): List[Computation] =
    c match {
      case Leaf(i) => List(Leaf(i))
      case Node(computation1, computation2, _) =>
        for {
          operation <- List(Add, Prod, Sub, Div)
          c1 <- useAllOperations(computation1)
          c2 <- useAllOperations(computation2)
          computation <- operation.possibleComputations(c1, c2)
          if computation.eval.isDefined
        } yield computation
    }

  private def allSums(values: List[Int]): List[Computation] =
    values match {
      case Nil => Nil
      case head :: tail =>
        val next = allSums(tail)
        Leaf(head) :: next ::: next.flatMap(allSums(_, head))
    }

  private def allSums(c: Computation, v: Int): List[Computation] =
    Node(Leaf(v), c) :: (c match {
      case _: Leaf => Nil
      case Node(l, r, _) => allSums(l, v).map(Node(_, r)) ::: allSums(r, v).map(Node(l, _))
    })


}
