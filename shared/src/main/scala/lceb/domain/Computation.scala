package lceb.domain

import Operation._


sealed trait Computation {
  def eval: Option[Int]
}

final case class Node(a: Computation, b: Computation, operation: Operation = Add) extends Computation {

  def eval: Option[Int] = (a.eval, b.eval) match {
    case (Some(ar), Some(br)) => operation.eval(ar, br)
    case _ => None
  }

  override def toString: String = s"($a $operation $b) => ${eval.getOrElse('?')}"
}

case class Leaf(i: Int) extends Computation {
  def eval: Option[Int] = Some(i)

  override val toString: String = i.toString
}
