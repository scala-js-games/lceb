package lceb.domain

import scala.util.Random

final case class Problem(target: Int, numbers: List[Int]) {
  def withTarget(t: Int): Problem = copy(target = t)

  def withNumber(position: Int)(number: Int): Problem = copy(numbers = numbers.updated(position, number))
}

object Problem {
  def random: Problem = Problem(Random.nextInt(500) + 500, Random.shuffle(List(25, 50, 100) ++ (1 to 10)).take(6))


}
