package lceb.domain

final case class Operation(commutative: Boolean, repr: Char, eval: (Int, Int) => Option[Int]) {
  override val toString = repr.toString

  def possibleComputations(a: Computation, b: Computation): List[Node] =
    if (commutative) List(Node(a, b, this))
    else List(Node(a, b, this), Node(b, a, this))
}

object Operation {

  val Add = Operation(true, '+', (a, b) => Some(a + b))
  val Prod = Operation(true, '*', (a, b) => Some(a * b))
  val Sub = Operation(false, '-', (a, b) => if (a > b) Some(a - b) else None)
  val Div = Operation(false, '/', (a, b) => if (b != 0 && a % b == 0) Some(a / b) else None)
}